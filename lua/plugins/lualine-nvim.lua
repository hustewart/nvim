local config = function()
  local theme = require("lualine.themes.gruvbox")
  theme.normal.c.bg = nil

  require("lualine").setup {
    options = {
      theme = theme,
      globalstatus = true, -- if set to false, new lualine in each pane
    },
    sections = {
      lualine_a = {
        {
          -- I had this at first but was too cluttered.
          -- Can see whole list of buffers with <leader>fb
          -- "buffers",
        }
      },
    }
  }
end

return {
    'nvim-lualine/lualine.nvim',
    dependencies = { 'nvim-tree/nvim-web-devicons' },
    lazy = false,
    config = config
}
