-- from https://github.com/folke/lazy.nvim
return {
  { "folke/neoconf.nvim", cmd = "Neoconf" },
  "folke/neodev.nvim",
}
