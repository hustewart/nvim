return {
  "nvim-tree/nvim-tree.lua",
  lazy = false,
  -- This table will be passed to the setup function.
  config = {
    filters = {
      dotfiles
    }
  }
}
