return {
	"tummetott/unimpaired.nvim",
	lazy = false,
	config = function()
		require('unimpaired').setup {
			-- add any options here or leave empty
		}
	end
}
