local ruby_lsp_executable = vim.fn.expand("~/") .. ".asdf/shims/ruby-lsp" -- Default path, users can override
local on_attach = require("util.lsp").on_attach
local diagnostic_signs = require("util.icons").diagnostic_signs
local typescript_organise_imports = require("util.lsp").typescript_organise_imports
local config = function()
	local cmp_nvim_lsp = require("cmp_nvim_lsp")
	local lspconfig = require("lspconfig")
	local capabilities = cmp_nvim_lsp.default_capabilities()

	-- Update capabilities to include formatting capability
	capabilities.textDocument.formatting = {
		dynamicRegistration = true -- Provide the correct structure
	}

	for type, icon in pairs(diagnostic_signs) do
		local hl = "DiagnosticSign" .. type
		vim.fn.sign_define(hl, { text = icon, texthl = hl, numhl = "" })
	end

	local gitlab_settings = {
		baseUrl = "https://gitlab.com",
		token = vim.env.GITLAB_TOKEN,
	}

	-- gitlab_lsp
	lspconfig.gitlab_lsp = {
		default_config = {
			name = "gitlab_lsp",
			cmd = { "gitlab-lsp", "--stdio" },
			filetypes = { "ruby", "go", "javascript" },
			single_file_support = true,
			root_dir = function(fname)
				-- return lspconfig.util.find_git_ancestor(fname)
				return vim.fs.dirname(vim.fs.find('.git', { path = fname, upward = true })[1])
			end,
			settings = gitlab_settings,
		},
		docs = {
			description = "GitLab Code Suggestions",
		},
		capabilities = capabilities,
		on_attach = on_attach,
	}

	-- rust
	lspconfig.rust_analyzer.setup {
		cmd = { "rustup", "run", "stable", "rust-analyzer" },
		capabilities = capabilities,
		on_attach = on_attach,
	}

	-- lua
	lspconfig.lua_ls.setup({
		capabilities = capabilities,
		on_attach = on_attach,
	})

	-- ruby
	lspconfig.ruby_lsp.setup({
		cmd = { ruby_lsp_executable },
		capabilities = capabilities,
		on_attach = on_attach
	})

	-- js
	lspconfig.biome.setup({
		on_attach = on_attach,
		capabilities = capabilities,
	})

	-- typescript
	lspconfig.ts_ls.setup({
		on_attach = on_attach,
		capabilities = capabilities,
		filetypes = {
			"typescript",
			"javascript",
			"typescriptreact",
			"javascriptreact",
		},
		commands = {
			TypeScriptOrganizeImports = typescript_organise_imports,
		},
		settings = {
			typescript = {
				indentStyle = "space",
				indentSize = 2,
			},
		},
		root_dir = lspconfig.util.root_pattern("package.json", "tsconfig.json", ".git"),
	})

	-- bash
	lspconfig.bashls.setup({
		capabilities = capabilities,
		on_attach = on_attach,
		filetypes = { "sh", "aliasrc" },
	})

	-- docker
	lspconfig.dockerls.setup({
		capabilities = capabilities,
		on_attach = on_attach,
	})
end

return {
	"neovim/nvim-lspconfig",
	config = config,
	lazy = false,
	dependencies = {
		"windwp/nvim-autopairs",
		"williamboman/mason.nvim",
		"hrsh7th/nvim-cmp",
		"hrsh7th/cmp-buffer",
		"hrsh7th/cmp-nvim-lsp",
	},
}
