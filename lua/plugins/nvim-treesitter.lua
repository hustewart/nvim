local config = function()
	require("nvim-treesitter.configs").setup({
		indent = {
			enable = false,
		},
		autotag = {
			enable = true,
		},
		ensure_installed = {
			"bash",
			"css",
			"dockerfile",
			"gitignore",
			"html",
			"javascript",
			"json",
			"lua",
			"markdown",
			"markdown",
			"python",
      "ruby",
			"rust",
			"solidity",
			"typescript",
			"vue",
			"yaml",
		},
		auto_install = true,
		highlight = {
			enable = true,
			additional_vim_regex_highlighting = true,
		},
	})
end

return {
	"nvim-treesitter/nvim-treesitter",
  lazy = false,
  config = config
}
