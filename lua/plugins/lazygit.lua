return {
  {
    "kdheepak/lazygit.nvim",
		keys= {
		  vim.keymap.set("n", "<leader>lg", ":LazyGit<CR>"),
		},
    cmd = {
      "LazyGit",
      "LazyGitConfig",
      "LazyGitCurrentFile",
      "LazyGitFilter",
      "LazyGitFilterCurrentFile",
    },
    -- optional for floating window border decoration
    dependencies = {
      "nvim-lua/plenary.nvim",
    },
  },
}
