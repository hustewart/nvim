--  I think this is some notification thing, need to read up.
return {
  "folke/noice.nvim",
  event = "VeryLazy",
  opts = {
    -- add any options here
  },
  dependencies = {
    -- if you lazy-load any plugin below, make sure to add proper `module="..."` entries
    "MunifTanjim/nui.nvim",
    -- OPTIONAL:
    --   `nvim-notify` is only needed, if you want to use the notification view.
    --   If not available, we use `mini` as the fallback
    --   This stuff below will add all that notification junk on the right side that we don't want.
    -- {
    --   "rcarriga/nvim-notify",
    --   config = function()
    --     require("notify").setup({
    --       background_color = "#000000"
    --     })
    --   end,
    -- },
  }
}
