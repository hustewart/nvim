local mapkey = require("util.keymapper").mapvimkey

-- Source nvim config
mapkey("<leader>sn", "source ~/.config/nvim/init.lua", "n", { silent = false })

-- Buffer Navigation
mapkey("<leader>bn", "bnext", "n")     -- Next buffer
mapkey("<leader>bp", "bprevious", "n") -- Prev buffer
mapkey("<leader>bb", "e #", "n")       -- Switch to Other Buffer
mapkey("<leader>`", "e #", "n")        -- Switch to Other Buffer

-- Directory Navigation
mapkey("<leader>e", "NvimTreeFindFile", "n")
mapkey("<leader>ec", "NvimTreeToggle", "n")

-- Show Full File-Path
mapkey("<leader>pa", "echo expand('%:p')", "n") -- Show Full File Path

-- Indenting
vim.keymap.set("v", "<", "<gv", { silent = true, noremap = true })
vim.keymap.set("v", ">", ">gv", { silent = true, noremap = true })

-- Create an autocommand group for terminal buffers
vim.api.nvim_create_augroup("custom-term-open", {})
vim.api.nvim_create_autocmd("TermOpen", {
  group = "custom-term-open",
  pattern = { "*" },
  callback = function()
    vim.opt_local.number = false
    vim.opt_local.relativenumber = false
    -- vim.opt_local.scrolloff = 0
    vim.opt_local.scrolloff = 999
  end,
})

-- Easily hit escape in terminal mode
vim.keymap.set("t", "<esc><esc>", function()
  print("Exiting terminal mode")
  vim.api.nvim_input("<C-\\><C-n>")
end, { silent = true, noremap = true })

-- Open a terminal at the bottom of the screen with a fixed height
vim.keymap.set("n", ",st", function()
  vim.cmd("new")                     -- Create a new horizontal split
  vim.cmd("wincmd J")                -- Move the new window to the bottom
  vim.api.nvim_win_set_height(0, 12) -- Set the height of the window
  vim.wo.winfixheight = true         -- Fix the window height
  vim.cmd("term")                    -- Open a terminal in the new window
end, { silent = true, noremap = true })
