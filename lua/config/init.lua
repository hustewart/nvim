-- This file is required by the main ~/.config/nvim/init.lua file that gets
-- invoked when nvim is started.
--
-- The vim.fn.stdpath('data') function is a Vimscript function provided by
-- Neovim that returns the path to different standard directories, and 'data'
-- specifically refers to the data directory.
--
--constructs the lazypath variable by appending '/lazy/lazy.nvim' to the path
--of the Neovim data directory. In other words, it points to a directory named
--'lazy.nvim' within the Neovim data directory. The resulting lazypath variable
--represents the location where the 'lazy.nvim' directory should be located in
--the Neovim runtime environment.
--
local lazypath = vim.fn.stdpath('data') .. '/lazy/lazy.nvim'

if not vim.loop.fs_stat(lazypath) then
  vim.fn.system({
    'git',
    'clone',
    '--filter=blob:none',
    'https://github.com/folke/lazy.nvim.git',
    '--branch=stable', -- latest sgable release
    lazypath,
  })
end
vim.opt.rtp:prepend(lazypath)

require('config.globals')
require('config.options')
require('config.keymaps')
require('config.autocommands')

-- just overriding some of the default configs from
-- https://github.com/folke/lazy.nvim configuration section
local opts = {
	defaults = {
		lazy = true,
	},
	install = {
		colorscheme = { 'gruvbox' }
	},
	rtp = {
		disabled_plugins = {
			'gzip',
			'matchit',
			'matchparen',
			'netrwPlugin',
			'tarPlugin',
			'tohtml',
			'tutor',
			'zipPlugin',
		}
	},
	change_detection = {
		notify = true,
	},
}

require('lazy').setup('plugins', opts)
