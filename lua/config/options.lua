local opt = vim.opt

-- Folding
-- vim.opt.foldmethod = "expr"
-- vim.opt.foldexpr = "v:lua.vim.treesitter.foldexpr()"
-- vim.opt.foldtext = ""
-- vim.opt.foldlevel = 99
-- vim.opt.foldlevelstart = 1
-- vim.opt.foldnestmax = 4

-- Tab / Indentation
opt.tabstop = 2        -- Sets the number of spaces each tab takes up.
opt.shiftwidth = 2     -- Sets the number of spaces for each level of indentation.
opt.softtabstop = 2    -- When in insert mode how many spaces is tab.
-- opt.expandtab = true   -- Converts tabs into spaces when opening a file.
opt.smartindent = true -- Auto indentation.
opt.wrap = false       -- We don't want wrapping.

-- Search
opt.incsearch = true -- As you add letters it will highlight all matches.
opt.ignorecase = true
opt.smartcase = true -- Ignore cases to begin with but where there's capitalization use it in search.
opt.hlsearch = false -- Don't highlight the search.

-- Appearance
opt.number = true          -- Show line numbers.
opt.relativenumber = false -- Make line numbers relative.
opt.termguicolors = true   -- Allows full color support in the terminal, required by some plugins.
opt.colorcolumn = '80'     -- Let me know when the text is too long.
-- Will keep a gutter and some space available in case there's an debug/linting
-- thing so we don't see a change in width.
opt.signcolumn = 'yes'
opt.cmdheight = 1 -- Command window height.
-- When searching up and down the screen, starts scrolling within this number.
opt.scrolloff = 10
-- Will show if only one item in menue, nothing inserted by default, nothing
-- selected by default.
opt.completeopt = 'menuone,noinsert,noselect'

-- Behavior
opt.hidden = true                   -- Change buffers without saving.
opt.errorbells = false              -- If set to true it will make a noise.
opt.swapfile = false                -- No swap files.
opt.backup = false                  -- No backup files.
opt.undofile = true                 -- We do want an undo file.
opt.backspace = 'indent,eol,start'  -- See comment directly above.
opt.splitright = true               -- On new vertical split, put new pane to the right.
opt.splitbelow = true               -- On new horizontal split, put new pane below.
opt.autochdir = false               -- Do no automatically change dir.
opt.iskeyword:append('-')           -- Allow words with dashes to be considered whole words.
opt.mouse:append("a")               -- Mouse always available.
opt.clipboard:append('unnamedplus') -- Allow copy/paste in and out of vim.
opt.modifiable = true               -- You can edit the buffer you're in by default.
opt.encoding = 'UTF-8'
-- Visual representation for cursor. Make it blink. This helps me find it.
opt.guicursor = 'n-v-c:block,' ..
		'i-ci-ve:block,' ..
		'r-cr:hor20,' ..
		'o:hor50,' ..
		'a:blinkwait700-blinkoff400-blinkon250-Cursor/lCursor,' ..
		'sm:block-blinkwait175-blinkoff150-blinkon175'
