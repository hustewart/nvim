-- Define a Lua function to format the current file with Biome
local function biome_format_write()
    -- Get the current file name
    local filename = vim.fn.expand('%')

    -- Run the Biome formatter with the --write option
    local command = string.format('npx @biomejs/biome format %s --write', filename)
    vim.fn.system(command)
end

-- Define a custom command in Lua to trigger the Biome formatting with --write option
vim.cmd[[
  command! -buffer BiomeFormatWrite :lua require('my_module').biome_format_write()
]]

-- Optionally define a keymap to trigger the command
vim.api.nvim_buf_set_keymap(0, 'n', '<leader>fw', ':BiomeFormatWrite<CR>', { noremap = true, silent = true })
